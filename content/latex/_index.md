---
Title: "Mi a LaTeX?"
Date: 2010-04-17T17:00:00+02:00
Draft: false
Author: "Meskó Balázs"
---

# Mi a LaTeX?
---

A [$\mathrm{\LaTeX}$][1] (<code>/ˈlɑːtɛx/</code>) egy szövegszedőrendszer,
amelyet elsősorban olyan dokumentumok, szakdolgozatok és tudományos publikációk
készítéséhez használnak, melyek sok képletet tartalmaznak. A rendszer első
verzióját Leslie Lamport készítette 1985-ben, azóta is folyamatosan fejlesztik,
és nagy ökoszisztéma nőtt a projekt köré. A LaTeX alapja a Donald Knuth által
fejlesztett [$\mathrm{\TeX}$][2] (<code>/ˈtɛx/</code>) volt, melyet 1978-ban
adott ki. A LaTeX valójában egy makrógyűjtemény, amely könnyebbé teszi a munkát.

A TeX és a LaTeX szabad szoftver, előbbi Knuth saját licenc alatt, utóbbi pedig
[LPPL licenc][3] alatt érhető el. Az egyes LaTeX-terjesztésekben és a CTAN-on
található csomagok is jellemzően ezt a licencet használják, de lehetnek
kivételek.

## Miben más a LaTeX?

A legtöbben ha dokumentumszerkesztésre gondolnak, akkor olyan szoftverekre
gondolnak, mint a LibreOffice Writer, a Microsoft Word, vagy a Google
Dokumentumok. Ezeket <abbr title="Azt látod, amit kapsz, hűen">alakhű</abbr>
szerkesztőnek is hívhatjuk (angol rövidítéssel 
<abbr title="What You See Is What You Get">WYSIWIG</abbr>). Az ilyen grafikus
programoknál nem különül el a tartalom és a formázás. A LaTeX ezzel
szemben gyökeresen más szemléletű, a dokumentum készítője leírja egy 
szövegfájlba, hogy mit szeretne kimenetként kapni, majd ráfuttatja a LaTeX-et,
és előállítja a végeredményt. Elsőre hátránynak tűnhet, hogy fel kell 
függeszteni a munkát az eredmény megtekintéséhez, de számos előnye van a LaTeX 
használatának:

 - A dokumentum szerkezetére és tartalmára lehet koncentrálni, a LaTeX 
   automatikusan biztosítja, hogy a tipográfiai (betűkészletek, betűméretek, 
   sormagasságok) és egyéb elrendezési beállítások konzisztensek legyenek a 
   megadott szabályokkal.
 - A LaTeX-ben a dokumentumok szerkezete látható a felhasználó számára, és 
   könnyedén átmásolható egy másik dokumentumba. Az alakhű szerkesztőknél
   sokszor nem egyértelmű, hogy egyes formázások hogyan készültek, és az is
   lehet, hogy közvetlenül lehetetlen is az átmásolásuk egy másik dokumentumba.
 - Az indexek, lábjegyzetek és hivatkozások könnyedén és automatikusan 
   előállíthatók.
 - A matematikai formulák könnyedén szedhetőek. (A minőségi matematika volt a 
   TeX egyik eredeti motivációja.)
 - Mivel dokumentum forrása egyszerű szöveg:
   - A dokumentumok forrás bármely szövegszerkesztőben elolvasható és 
     megérthető, ellentétben a dokumentumszerkesztők bináris vagy XML-alapú 
     formátumaival.
   - A táblázatok, ábrák, egyenletek stb. előállíthatók bármilyen programozási 
     nyelven.
   - A módosítások jól követketők [verziókezelő rendszerekben][4].
 - Egyes tudományos folyóiratok kizárólagosan LaTeX dokumentumokat fogadnak el, 
   vagy legalább is erősen javasolják azt. A kiadók gyakran biztosítanak LaTeX 
   sablonokat.

A forrásfájl LaTeX programmal (más néven motor) történő feldolgozás során
számos kimenet készíthető. A LaTeX natívan a DVI és PDF formátumokat támogatja, 
de más szoftverekkel könnyedén átalakítható képpé vagy PostScript-fájllá.

## LaTeX terjesztések és motorok

A LaTeX a különböző Linux disztribúciókhoz hasonlóan többféle terjesztésként
érhető el, a két legnagyobb terjesztés a [TeX Live][5] és a [MiKTeX][6] -- 
előbbi mondhatni a *de facto* szabvány, míg utóbbi főleg Windowson népszerű,
mert kényelmesebb a telepítése, és rendelkezik 64 bites változattal is.

<!--## Szerkesztőprogramok

Tetszőleges szövegszerkesztő használható LaTeX dokumentumok készítéséhez, de
érdemes kifejezetten erre a célra fejlesztett szerkesztőprogramot használni. Az
elérhető -->


[1]: https://www.latex-project.org/
[2]: http://tug.org/
[3]: https://www.latex-project.org/lppl/
[4]: https://hu.wikipedia.org/wiki/Verzi%C3%B3kezel%C3%A9s
[5]: https://www.tug.org/texlive/
[6]: https://miktex.org/

